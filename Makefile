.PHONY:	all bash tools posh git vim tmux
.SILENT:

all: tools bash posh git vim tmux

tools:
	sudo apt-get update && sudo apt-get install zip unzip jq vim tmux neofetch

bash:
	./bin/setup-bash

posh:
	./bin/setup-posh

git:
	./bin/setup-git

vim:
	./bin/setup-vim

tmux:
	./bin/setup-tmux
