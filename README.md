# workstation-setup

Automating setting up my debian-based workstation.

These scripts assume a debian based system (`apt-get` in particular) and the bash shell. Assumes `make` is available, as well as, of course, `git`.

## tools

```bash
make tools
```

will install `zip` and `unzip` and whatever else I figure I need on a regular basis.
Some of these tools will be needed for the rest of the scripts here. I know for sure that `oh-my-posh` needs `unzip`, but I didn't make it a dependency in the `Makefile`.

## oh-my-posh

You need to install a nerd font so that you get all the good out of oh-my-posh. I've been using *CascaydiaCove*. Download it from [nerdfonts.com](https://www.nerdfonts.com/font-downloads)

Finally, run

```bash
make posh
```

to install oh-my-posh, along with the custom style definition. This script will also add the setup line to your .bashrc if it isn't already there.

Then you'll need to set up the nerd font in both your terminal and your editor. See the next couple of sections for details.

## VS Code

### Font

Since we just set up oh-my-posh and installed a nerd font, let's set up VSCode to use it.

Under Settings, search for "font" and edit the **Editor: Font Family** to contain `'CaskaydiaCove Nerd Font', Consolas, 'Courier New', monospace`

Make sure that **Terminal > Integrated: Font Family** is blank so that it will inherit its font from the editor setting.

### Color Theme

I currently like **Dark Modern**, but I've also spent a lot of time with **Cobalt 2**, which would need to be installed.

## MS Terminal

### Terminal Font

Since you've already installed your nerd font, open **Profiles|Defaults|Text** and **Font Face** to `CaskaydiaCove Nerd Font`.

### Terminal Color Scheme

There's a great list of color schemes for MS Terminal at <https://windowsterminalthemes.dev/>. I like this one, named `Zeonica`. It's based off of a VS Code theme, but I don't like it for VS Code.

To update terminal, open the JSON file (using the gear in the bottom left) and add it to the **schemes** list and don't forget your comma separator.

```json
{
  "name": "Zeonica",
  "black": "#0C0C0C",
  "red": "#CC2929",
  "green": "#21C221",
  "yellow": "#D6C315",
  "blue": "#3E31F5",
  "purple": "#D918B9",
  "cyan": "#13D4D4",
  "white": "#B2B2B2",
  "brightBlack": "#686868",
  "brightRed": "#FF6E6E",
  "brightGreen": "#6BFF6B",
  "brightYellow": "#FFFF6B",
  "brightBlue": "#737CFF",
  "brightPurple": "#FF70FF",
  "brightCyan": "#7FFFFF",
  "brightWhite": "#FFFFFF",
  "background": "#01001D",
  "foreground": "#FFFFFF",
  "selectionBackground": "#242272",
  "cursorColor": "#FFFFFF"
}
```

Then under **Settings|Defaults|Appearance|Text**, set **Color Scheme** to *Zeonica*.

## git

First, create your ssh key pair and update gitlab/github with it.

Now execute

```sh
make git
```

to add some base configuration as well as some aliases that I like.

Go read the GitHub or GitLab docs to add code signing...

## vim

Execute

```sh
make vim
```

To set up vim with a plugin manager as well as a color theme and status line theme.

## tmux

Tmux needs to be installed, which is done with `make tools`, which the tmux job does **not** depend on, so you either must install it manually or with `make tools` or with `make all` before running the tmux setup script individually.

In the configuration, I'm forcing 24bit color mode to true rather than auto. The MS Terminal seems to handle it fine and I don't wanna go figure out who is setting the TERM environment varialble to 256 colors. If you see anything weird where colors are concerned, this might be the culprit.

```sh
make tmux
```

This rule sets up tmux with some nice theming and most importantly, the ctrl-space trigger in addition to the ctrl-b trigger, which seems to be easier on the hands.
