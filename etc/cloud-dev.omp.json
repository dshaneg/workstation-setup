{
  "$schema": "https://raw.githubusercontent.com/JanDeDobbeleer/oh-my-posh/main/themes/schema.json",
  "version": 2,
  "final_space": true,
  "console_title_template": "{{ .HostName }} in {{ .PWD }}",
  "blocks": [
    {
      "type": "prompt",
      "alignment": "left",
      "segments": [
        {
          "type": "path",
          "style": "powerline",
          "powerline_symbol": "\ue0b0",
          "foreground": "p:default-fg",
          "background": "p:path-bg",
          "template": " {{ .Path }} ",
          "properties": {
            "home_icon": "~",
            "mapped_locations": {
              "~/src/dshaneg/katas": "🥋"
            },
            "style": "full"
          }
        },
        {
          "type": "git",
          "style": "powerline",
          "powerline_symbol": "\ue0b0",
          "foreground": "p:default-fg",
          "background": "p:git-bg-synced",
          "background_templates": [
            "{{ if or (.Working.Changed) (.Staging.Changed) }}p:git-bg-changed{{ end }}",
            "{{ if and (gt .Ahead 0) (gt .Behind 0) }}p:git-bg-aheadbehind{{ end }}",
            "{{ if gt .Ahead 0 }}p:git-bg-ahead{{ end }}",
            "{{ if gt .Behind 0 }}p:git-bg-behind{{ end }}"
          ],
          "template": " {{ .UpstreamIcon }}{{ .HEAD }} {{ .BranchStatus }}{{ if .Working.Changed }} \uf044 {{ .Working.String }}{{ end }}{{ if and (.Working.Changed) (.Staging.Changed) }} |{{ end }}{{ if .Staging.Changed }} \uf046 {{ .Staging.String }}{{ end }}{{ if gt .StashCount 0 }} \uf692 {{ .StashCount }}{{ end }} ",
          "properties": {
            "branch_icon": "\uf126 ",
            "fetch_stash_count": true,
            "fetch_status": true,
            "fetch_upstream_icon": true
          }
        },
        {
          "type": "executiontime",
          "style": "powerline",
          "powerline_symbol": "\ue0b0",
          "foreground": "p:default-fg",
          "background": "p:executiontime-bg",
          "template": " <p:prefix-fg></>{{ .FormattedMs }} ",
          "properties": {
            "style": "austin",
            "threshold": 100
          }
        },
        {
          "type": "status",
          "style": "powerline",
          "powerline_symbol": "\ue0b0",
          "foreground": "p:exit-fg-success",
          "foreground_templates": [
            "{{ if gt .Code 0 }}p:default-fg{{ end }}"
          ],
          "background": "p:exit-bg-success",
          "background_templates": [
            "{{ if gt .Code 0 }}p:exit-bg-fail{{ end }}"
          ],
          "template": " {{ if gt .Code 0 }}{{ .Code }} {{ reason .Code }}{{ else }}✔{{ end }} ",
          "properties": {
            "always_enabled": false
          }
        }
      ]
    },
    {
      "type": "prompt",
      "alignment": "right",
      "segments": [
        {
          "type": "dotnet",
          "style": "powerline",
          "powerline_symbol": "\ue0b0",
          "foreground": "#ffc600",
          "background": "#234E6D",
          "template": " {{ if .Unsupported }}\uf071{{ else }}{{ .Full }}{{ end }} \ue77f "
        },
        {
          "type": "node",
          "style": "powerline",
          "powerline_symbol": "\ue0b0",
          "foreground": "#ffffff",
          "background": "#689f63",
          "template": " {{ if .PackageManagerIcon }}{{ .PackageManagerIcon }} {{ end }}{{ .Full }} \uf898 ",
          "properties": {
            "fetch_version": true
          }
        },
        {
          "type": "go",
          "style": "powerline",
          "powerline_symbol": "\ue0b0",
          "foreground": "#111111",
          "background": "#00acd7",
          "template": " {{ if .Error }}{{ .Error }}{{ else }}{{ .Full }}{{ end }} \ue627 ",
          "properties": {
            "fetch_version": true
          }
        },
        {
          "type": "julia",
          "style": "powerline",
          "powerline_symbol": "\ue0b0",
          "foreground": "#111111",
          "background": "#4063D8",
          "template": " {{ if .Error }}{{ .Error }}{{ else }}{{ .Full }}{{ end }} \ue624 ",
          "properties": {
            "fetch_version": true
          }
        },
        {
          "type": "python",
          "style": "powerline",
          "powerline_symbol": "\ue0b0",
          "foreground": "#111111",
          "background": "#FFDE57",
          "template": " {{ .Full }} \ue235 ",
          "properties": {
            "display_mode": "files",
            "fetch_virtual_env": true
          }
        },
        {
          "type": "ruby",
          "style": "powerline",
          "powerline_symbol": "\ue0b0",
          "foreground": "#ffffff",
          "background": "#AE1401",
          "template": " {{ if .Error }}{{ .Error }}{{ else }}{{ .Full }}{{ end }} \ue791 ",
          "properties": {
            "display_mode": "files",
            "fetch_version": true
          }
        },
        {
          "type": "azfunc",
          "style": "powerline",
          "powerline_symbol": "\ue0b0",
          "foreground": "#ffffff",
          "background": "#FEAC19",
          "template": " {{ if .Error }}{{ .Error }}{{ else }}{{ .Full }}{{ end }} \uf0e7",
          "properties": {
            "display_mode": "files",
            "fetch_version": false
          }
        },
        {
          "type": "root",
          "style": "powerline",
          "powerline_symbol": "\ue0b0",
          "foreground": "#111111",
          "background": "#ffff66",
          "template": " \uf0e7 ",
          "properties": {
            "root_icon": "\uf0ad"
          }
        }
      ]
    },
    {
      "type": "prompt",
      "alignment": "left",
      "segments": [
        {
          "type": "aws",
          "style": "powerline",
          "powerline_symbol": "\ue0b0",
          "foreground": "p:default-fg",
          "foreground_templates": [
            "{{if contains \"admin\" .Profile}}p:hilite-fg{{end}}"
          ],
          "background": "p:aws-bg-dev",
          "background_templates": [
            "{{if contains \"prod\" .Profile}}p:aws-bg-prod{{end}}"
          ],
          "template": " <p:prefix-fg>\u2003</>{{.Profile}} ",
          "properties": {
            "display_default": true
          }
        },
        {
          "type": "kubectl",
          "style": "powerline",
          "powerline_symbol": "\ue0b0",
          "foreground": "p:default-fg",
          "foreground_templates": [
            "{{if contains \"admin\" .Context}}p:hilite-fg{{end}}"
          ],
          "background": "p:k8s-bg-dev",
          "background_templates": [
            "{{if contains \"prod\" .Context}}p:k8s-bg-prod{{else if contains \"qa\" .Context}}p:k8s-bg-qa{{end}}"
          ],
          "template": " <p:prefix-fg>󱃾\u2003</>{{.Context}} "
        },
        {
          "type": "session",
          "style": "plain",
          "foreground": "p:session-fg",
          "template": " {{ if .SSHSession }}󰢹 {{ end }}{{ .UserName }}@{{ .HostName }} $"
        }
      ],
      "newline": true
    }
  ],
  "palette": {
    "aws-bg-dev": "#0d3a58",
    "aws-bg-prod": "#f44542",
    "default-fg": "#ddd",
    "executiontime-bg": "#8800dd",
    "exit-bg-fail": "#cc2222",
    "exit-bg-success": "#111",
    "exit-fg-success": "#3ad900",
    "git-bg-ahead": "#0088ff",
    "git-bg-aheadbehind": "#f26d50",
    "git-bg-behind": "#c4a000",
    "git-bg-changed": "#234E6D",
    "git-bg-synced": "#4e9a06",
    "hilite-fg": "#ffff00",
    "k8s-bg-dev": "#234E6D",
    "k8s-bg-prod": "#ff628c",
    "k8s-bg-qa": "#027dff",
    "path-bg": "#333",
    "prefix-fg": "#ddd",
    "session-fg": "#3ad900"
  }
}
